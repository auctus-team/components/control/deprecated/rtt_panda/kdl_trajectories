/*
 * Copyright 2019 <copyright holder> <email>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include <kdl/chainiksolverpos_nr_jl.hpp>
#include <kdl/chainfksolvervel_recursive.hpp>
#include <kdl/chainfksolverpos_recursive.hpp>
#include <kdl/chain.hpp>
#include <kdl/frames_io.hpp>
#include <kdl/jacobian.hpp>
#include <kdl/chainjnttojacsolver.hpp>
#include <kdl/jntspaceinertiamatrix.hpp>
#include <kdl/chaindynparam.hpp>
#include <kdl/path_cyclic_closed.hpp>
#include <kdl/path_line.hpp>
#include <kdl/path_circle.hpp>
#include <kdl/trajectory_composite.hpp>
#include <kdl/trajectory_stationary.hpp>
#include <kdl/trajectory.hpp>
#include <kdl/trajectory_segment.hpp>
#include <kdl/rotational_interpolation_sa.hpp>
#include <kdl/utilities/error.h>
#include <kdl_conversions/kdl_msg.h>
#include <kdl/velocityprofile_trap.hpp>
#include <kdl/velocityprofile_spline.hpp>

#include <kdl_trajectories/PublishTraj.h>
#include <kdl_trajectories/TrajProperties.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/PoseArray.h>
#include <geometry_msgs/PoseStamped.h>
#include <nav_msgs/Path.h>

#include <ros/node_handle.h>
#include <realtime_tools/realtime_publisher.h>
#include <kdl_trajectories/UpdateWaypoints.h>

#include <vector>
#include <string>
#include <Eigen/Dense>

struct MovementElement
{
  KDL::Frame frame;
  std::string movetype = "";
  std::vector<double> options;
};

/**
 * Generates a Cartesian space trajectory with an underlying velocity profile
 */
class TrajectoryGenerator
{
private:
  KDL::Trajectory_Composite *ctraject;
  std::vector<MovementElement> movement_list;
  std::vector<double> movement_durations;
  std::vector<double> movement_vmax_old;
  double init_dur;
  double accmax = 1.0;
  Eigen::Vector3d u = Eigen::Vector3d(0, 1, 0);

public:
  
  bool in_loop = false; 

  /**
   * \brief
   * Load a csv file containing a desired velocity
   * \param x_curr The robot current pose
   */
  void Load(std::string csv_file_name);

  /**
   * \brief
   * Builds a loaded Cartesian space trajectory.
   * \param x_curr The robot current pose
   * \param verbose optional. Display some information on the trajectory if True
   * \return true if trajectory could be build
   */
  bool Build(KDL::Frame x_curr, bool verbose);

  /**
   * \brief
   * Computes the direction of motion along the trajectory in dt at a given time
   * \param time The current time
   * \param dt The small time step to the next pose
   */
  Eigen::Vector3d DirectionOfMotion(double time, double dt);

  /**
   * \brief
   * Get the duration of the trajectory
   */
  double Duration();

  /**
   * \brief
   * Get the time until reaching the first point along the trajectory
   */
  double ResetDuration();

  /**
   * \brief
   * Set the maximum acceleration along the trajectory profile
   * \param amax The new maximal acceleration
   */
  void SetAccMax(double amax);

  /**
   * \brief
   * Get the desired Pose
   */
  KDL::Frame Pos(double time);

  /**
   * \brief
   * Get the desired Twist
   */
  KDL::Twist Vel(double time);

  /**
   * \brief
   * Get the desired Acceleration
   */
  KDL::Twist Acc(double time);
};