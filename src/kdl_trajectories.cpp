#include <kdl_trajectories/kdl_trajectories.hpp>

void TrajectoryGenerator::Load(std::string csv_file_name)
{
  movement_list.clear();

  // Get the csv file from path
  std::ifstream csv_file(csv_file_name);
  std::string line;
  // Iterate on each line of the csv file
  while (std::getline(csv_file, line, '\n'))
  {
    ROS_INFO_STREAM("line:  " << line);
    std::istringstream iss(line);
    std::string cmd_type;
    if (!(iss >> cmd_type))
    {
      break;
    }
    else
    {
      // If element is "LOOP", add it to the element list
      if (cmd_type.compare("LOOP") == 0)
      {
        MovementElement list_element;
        list_element.movetype = cmd_type;
        movement_list.push_back(list_element);
        ROS_INFO_STREAM("Adding LOOP");
      }
      // If element is "MOVELR" or "MOVEL", get the corresponding frame and the desired velocity and add it to the element list
      else if (cmd_type.compare("MOVELR") == 0 || cmd_type.compare("MOVEL") == 0 )
      { 
        double x, y, z, r, p, yaw, vel;
        if (!(iss >> x >> y >> z >> r >> p >> yaw >> vel))
        {
          break;
        }
        else
        {
          MovementElement list_element;
          list_element.movetype = cmd_type;
          list_element.options.push_back(vel);
          list_element.frame = KDL::Frame(KDL::Rotation::RPY(r, p, yaw), KDL::Vector(x, y, z));
          movement_list.push_back(list_element);
          ROS_INFO_STREAM("Adding "<<cmd_type <<" frame");
        }
      }
      // If element is "MOVELS", get the corresponding frame and the desired motion duration and add it to the element list
      else if (cmd_type.compare("MOVELS") == 0)
      {
        double x, y, z, r, p, yaw, dur;
        if (!(iss >> x >> y >> z >> r >> p >> yaw >> dur))
        {
          break;
        }
        else
        {
          MovementElement list_element;
          list_element.movetype = cmd_type;
          list_element.options.push_back(dur);
          list_element.frame = KDL::Frame(KDL::Rotation::RPY(r, p, yaw), KDL::Vector(x, y, z));
          movement_list.push_back(list_element);
          ROS_INFO_STREAM("Adding MOVELS frame");
        }
      }
      // If cmd_type is "MOVEC" add a new circular movement and the desired velocity and add it to the list_element
      else if (cmd_type.compare("MOVEC") == 0)
      {
        double x, y, z, r, p, yaw, r_end, p_end, yaw_end, px, py, pz, vx, vy, vz, alpha, vel;
        if (!(iss >> x >> y >> z >> r >> p >> yaw >> r_end >> p_end >> yaw_end >> px >> py >> pz >> vx >> vy >> vz >>
              alpha >> vel))
        {
          break;
        }
        else
        {
          MovementElement list_element;
          list_element.movetype = cmd_type;
          list_element.options.push_back(r_end);
          list_element.options.push_back(p_end);
          list_element.options.push_back(yaw_end);
          list_element.options.push_back(px);
          list_element.options.push_back(py);
          list_element.options.push_back(pz);
          list_element.options.push_back(vx);
          list_element.options.push_back(vy);
          list_element.options.push_back(vz);
          list_element.options.push_back(alpha);
          list_element.options.push_back(vel);
          list_element.frame = KDL::Frame(KDL::Rotation::RPY(r, p, yaw), KDL::Vector(x, y, z));
          movement_list.push_back(list_element);
          ROS_INFO_STREAM("Adding MOVEC frame");
        }
      }
      // If cmd_type is "MOVECR" add a new repeat circular movement and the desired velocity and add it to the list_element
      else if (cmd_type.compare("MOVECR") == 0)
      {
        double x, y, z, r, p, yaw, r_end, p_end, yaw_end, px, py, pz, vx, vy, vz, alpha, vel, repeat;
        if (!(iss >> x >> y >> z >> r >> p >> yaw >> r_end >> p_end >> yaw_end >> px >> py >> pz >> vx >> vy >> vz >>
              alpha >> vel >> repeat))
        {
          break;
        }
        else
        {
          MovementElement list_element;
          list_element.movetype = cmd_type;
          list_element.options.push_back(r_end);
          list_element.options.push_back(p_end);
          list_element.options.push_back(yaw_end);
          list_element.options.push_back(px);
          list_element.options.push_back(py);
          list_element.options.push_back(pz);
          list_element.options.push_back(vx);
          list_element.options.push_back(vy);
          list_element.options.push_back(vz);
          list_element.options.push_back(alpha);
          list_element.options.push_back(vel);
          list_element.options.push_back(repeat);
          list_element.frame = KDL::Frame(KDL::Rotation::RPY(r, p, yaw), KDL::Vector(x, y, z));
          movement_list.push_back(list_element);
          ROS_INFO_STREAM("Adding MOVEC frame");
        }
      }
      // If cmd_type is "WAIT" add a pause 
      else if (cmd_type.compare("WAIT") == 0)
      {
        double w_time;
        if (!(iss >> w_time))
        {
          break;
        }
        else
        {
          MovementElement list_element;
          list_element.movetype = cmd_type;
          list_element.options.push_back(w_time);
          KDL::Frame frame;
          list_element.frame = frame;
          movement_list.push_back(list_element);
          ROS_INFO_STREAM("Adding WAIT time:  " << w_time);
        }
      }
    }
  }
}

bool TrajectoryGenerator::Build(KDL::Frame x_curr, bool verbose)
{
  ctraject = new KDL::Trajectory_Composite();
  movement_durations.clear();

  // Default settings
  double eqradius = 0.01;
  double vmax = 0.1;  // default value

  // Set frame
  KDL::Frame frame = KDL::Frame(x_curr.M, x_curr.p);
  KDL::Frame new_frame;
  KDL::Frame end_frame;

  // Velocity profile
  KDL::VelocityProfile *velpref;
  KDL::VelocityProfile_Spline *velpref_spline;

  // Paths
  KDL::Path_Line *path_line;
  KDL::Path_Circle *path_circle;
  KDL::Path_Cyclic_Closed *path_cyclic_closed;

  //------------------------------------
  // Iterate over items in movement_list
  //------------------------------------
  bool on_path = false;
  in_loop = false;
  MovementElement new_movement;
  for (int move = 0; move < movement_list.size(); move++)
  {
    new_movement = movement_list[move];
    new_frame = KDL::Frame(new_movement.frame.M, new_movement.frame.p);

    //-------------------------
    // MOVELR to path segment
    //-------------------------
    if (in_loop && !on_path)
    {
      if (new_movement.movetype.compare("MOVEL") == 0 || new_movement.movetype.compare("MOVELS") == 0 ||
          new_movement.movetype.compare("MOVEC") == 0 || new_movement.movetype.compare("MOVECR") == 0)
      {
        double vmax_default = 0.1;  // default value
        vmax = vmax_default;

        // Line path
        KDL::Frame start_frame = KDL::Frame(movement_list[move].frame.M, movement_list[move].frame.p);
        KDL::Path_Line *start_path =
            new KDL::Path_Line(frame, start_frame, new KDL::RotationalInterpolation_SingleAxis(), eqradius);

        // Add trajectory segment with velocity profile
        velpref = new KDL::VelocityProfile_Trap(vmax, accmax);
        velpref->SetProfile(0, start_path->PathLength());  // Set velocity profile from start and end positions
        ctraject->Add(new KDL::Trajectory_Segment(start_path, velpref));
        init_dur = ctraject->Duration();                    // Save initial time for resetting trajectory
        movement_durations.push_back(velpref->Duration());  // Duration at end of movement
        movement_vmax_old.push_back(vmax);

        if (verbose)
        {
          ROS_INFO_STREAM("Current frame:  " << frame);
          ROS_INFO_STREAM("Add path frame:  " << start_frame);
          ROS_INFO_STREAM("Velocity:  " << vmax);
          ROS_INFO_STREAM("movement_durations: " << movement_durations.back());
          ROS_INFO_STREAM("init_dur: " << init_dur);
          ROS_INFO_STREAM("Path to trajectory finished");
        }
        // Update frame
        frame = KDL::Frame(start_frame.M, start_frame.p);
        on_path = true;
      }
    }

    //------------------------------------------------------
    // LOOP start
    //------------------------------------------------------
    if (new_movement.movetype.compare("LOOP") == 0)
    {
      in_loop = true;
      if (verbose)
      {
        ROS_INFO_STREAM("Start LOOP");
      }
    }
    //------------------------------------------------------
    // MOVEL linear segment (trapezoidal velocity profile)
    //------------------------------------------------------
    else if (new_movement.movetype.compare("MOVEL") == 0)
    {
      // Read movement values
      vmax = new_movement.options[0];

      // Line path
      path_line = new KDL::Path_Line(frame, new_frame, new KDL::RotationalInterpolation_SingleAxis(), eqradius);

      // Add trajectory segment with velocity profile
      velpref = new KDL::VelocityProfile_Trap(vmax, accmax);
      velpref->SetProfile(0, path_line->PathLength());  // Set velocity profile from start and end positions
      ctraject->Add(new KDL::Trajectory_Segment(path_line, velpref));
      movement_durations.push_back(velpref->Duration());  // Duration at end of movement
      movement_vmax_old.push_back(vmax);

      if (verbose)
      {
        ROS_INFO_STREAM("Add MOVEL to frame:  " << new_frame);
        ROS_INFO_STREAM("Velocity:  " << vmax);
        ROS_INFO_STREAM("movement_durations: " << movement_durations.back());
      }
      // Update frame
      end_frame = KDL::Frame(ctraject->Pos(ctraject->Duration()));
      frame = KDL::Frame(end_frame.M, end_frame.p);
    }
    //------------------------------------------------------
    // MOVELS linear segment (spline velocity profile)
    //------------------------------------------------------
    else if (new_movement.movetype.compare("MOVELS") == 0)
    {
      // Read movement values
      double duration = new_movement.options[0];

      // Line path
      path_line = new KDL::Path_Line(frame, new_frame, new KDL::RotationalInterpolation_SingleAxis(), eqradius);

      // Add trajectory segment with velocity profile
      velpref_spline = new KDL::VelocityProfile_Spline();
      velpref_spline->SetProfileDuration(0.0, 0.0, 0.0, path_line->PathLength(), 0.0, 0.0, duration);
      ctraject->Add(new KDL::Trajectory_Segment(path_line, velpref_spline));
      movement_durations.push_back(velpref_spline->Duration());  // Duration at end of movement

      if (verbose)
      {
        ROS_INFO_STREAM("Add MOVELS to frame:  " << new_frame);
        ROS_INFO_STREAM("duration:  " << duration);
        ROS_INFO_STREAM("movement_durations: " << movement_durations.back());
      }
      // Update frame
      end_frame = KDL::Frame(ctraject->Pos(ctraject->Duration()));
      frame = KDL::Frame(end_frame.M, end_frame.p);
    }
    //------------------------------------------------------
    // MOVEC circular segment (trapezoidal velocity profile)
    //------------------------------------------------------
    else if (new_movement.movetype.compare("MOVEC") == 0)
    {
      // Read movement values
      KDL::Rotation R_base_end = KDL::Rotation::RPY(new_movement.options[0], new_movement.options[1],
                                                    new_movement.options[2]);  // Orientation at end of path
      KDL::Vector V_base_center(new_movement.options[3], new_movement.options[4],
                                new_movement.options[5]);  // Circle center position
      KDL::Vector V_base_p(new_movement.options[6], new_movement.options[7],
                           new_movement.options[8]);  // Circle axis vector
      double alpha = new_movement.options[9];
      vmax = new_movement.options[10];

      // Circle path
      path_circle = new KDL::Path_Circle(frame, V_base_center, V_base_p, R_base_end, alpha,
                                         new KDL::RotationalInterpolation_SingleAxis(), eqradius);

      // Add trajectory segment with velocity profile
      velpref = new KDL::VelocityProfile_Trap(vmax, accmax);
      velpref->SetProfile(0, path_circle->PathLength());  // Set velocity profile from start and end positions
      ctraject->Add(new KDL::Trajectory_Segment(path_circle, velpref));
      movement_durations.push_back(velpref->Duration());  // Duration at end of movement
      movement_vmax_old.push_back(vmax);

      if (verbose)
      {
        ROS_INFO_STREAM("Add MOVEC to frame:  " << new_frame);
        ROS_INFO_STREAM("V_base_center:  " << V_base_center);
        ROS_INFO_STREAM("V_base_p:  " << V_base_p);
        ROS_INFO_STREAM("R_base_end:  " << R_base_end);
        ROS_INFO_STREAM("alpha:  " << alpha);
        ROS_INFO_STREAM("Velocity:  " << vmax);
        ROS_INFO_STREAM("movement_durations: " << movement_durations.back());
      }

      // Update frame
      end_frame = KDL::Frame(ctraject->Pos(ctraject->Duration()));
      frame = KDL::Frame(end_frame.M, end_frame.p);
    }
    //-------------------------------------------------------------------
    // MOVECR repeatitive circular segment (trapezoidal velocity profile)
    //-------------------------------------------------------------------
    else if (new_movement.movetype.compare("MOVECR") == 0)
    {
      // Read movement values
      KDL::Rotation R_base_end = KDL::Rotation::RPY(new_movement.options[0], new_movement.options[1],
                                                    new_movement.options[2]);  // Orientation at end of path
      KDL::Vector V_base_center(new_movement.options[3], new_movement.options[4],
                                new_movement.options[5]);  // Circle center position
      KDL::Vector V_base_p(new_movement.options[6], new_movement.options[7],
                           new_movement.options[8]);  // Circle axis vector
      double alpha = new_movement.options[9];
      vmax = new_movement.options[10];
      int repeat = static_cast<int>(new_movement.options[11]);

      // Cyclic path
      path_cyclic_closed =
          new KDL::Path_Cyclic_Closed(new KDL::Path_Circle(frame, V_base_center, V_base_p, R_base_end, alpha,
                                                           new KDL::RotationalInterpolation_SingleAxis(), eqradius),
                                      repeat);

      // Add trajectory segment with velocity profile
      velpref = new KDL::VelocityProfile_Trap(vmax, accmax);
      velpref->SetProfile(0, path_cyclic_closed->PathLength());  // Set velocity profile from start and end positions
      ctraject->Add(new KDL::Trajectory_Segment(path_cyclic_closed, velpref));
      movement_durations.push_back(velpref->Duration());  // Duration at end of movement
      movement_vmax_old.push_back(vmax);

      if (verbose)
      {
        ROS_INFO_STREAM("Add MOVEC to frame:  " << new_frame);
        ROS_INFO_STREAM("V_base_center:  " << V_base_center);
        ROS_INFO_STREAM("V_base_p:  " << V_base_p);
        ROS_INFO_STREAM("R_base_end:  " << R_base_end);
        ROS_INFO_STREAM("alpha:  " << alpha);
        ROS_INFO_STREAM("Velocity:  " << vmax);
        ROS_INFO_STREAM("repeat:  " << repeat);
        ROS_INFO_STREAM("movement_durations: " << movement_durations.back());
      }

      // Update frame
      end_frame = KDL::Frame(ctraject->Pos(ctraject->Duration()));
      frame = KDL::Frame(end_frame.M, end_frame.p);
    }
    //------------------------
    // WAIT wait time segment
    //------------------------
    else if (new_movement.movetype.compare("WAIT") == 0)
    {
      // Read movement values
      double wait_time = new_movement.options[0];

      // Add trajectory segment
      ctraject->Add(new KDL::Trajectory_Stationary(wait_time, frame));
      movement_durations.push_back(wait_time);  // Duration at end of movement

      if (verbose)
      {
        ROS_INFO_STREAM("Wait time:  " << wait_time);
        ROS_INFO_STREAM("movement_durations: " << movement_durations.back());
        ROS_INFO_STREAM("Wait frame:  " << frame);
      }
    }
    else
    {
      if (verbose)
      {
        ROS_WARN("trajectory movetype not supported");
        return false;
      }
      exit(EXIT_FAILURE);
    }
  }
  return true;
}

Eigen::Vector3d TrajectoryGenerator::DirectionOfMotion(double time, double dt)
{
  KDL::Frame x_traj = ctraject->Pos(time);            // Get next point along the trajectory
  KDL::Frame x_traj_next = ctraject->Pos(time + dt);  // Get next point along the trajectory
  if (x_traj_next != x_traj)
  {
    KDL::Vector dir_ = KDL::diff(x_traj_next.p, x_traj.p);
    double dir_n = dir_.Normalize();
    if (dir_n != 0)
    {
      for (int i = 0; i < 3; ++i)
        u(i) = dir_(i);
    }
  }  // Else keep the previous direction
  // If needed we can filter the direction to smooth it

  return u;
}

double TrajectoryGenerator::Duration()
{
  return ctraject->Duration();
}

double TrajectoryGenerator::ResetDuration()
{
  return init_dur;
}

KDL::Frame TrajectoryGenerator::Pos(double time)
{
  return ctraject->Pos(time);
}

KDL::Twist TrajectoryGenerator::Vel(double time)
{
  return ctraject->Vel(time);
}

KDL::Twist TrajectoryGenerator::Acc(double time)
{
  return ctraject->Acc(time);
}

void TrajectoryGenerator::SetAccMax(double amax)
{
  accmax = amax;
}

