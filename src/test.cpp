#include <kdl_trajectories/kdl_trajectories.hpp>
#include <kdl_trajectories/PublishTraj.h>
#include <kdl_trajectories/TrajProperties.h>


int main (int  argc, char** argv)
{
    KDLTrajectories trajectory;

    KDL::Frame X_curr_;
    std::string csv_file_name = "trajectories/arc.csv";
    trajectory.Load(csv_file_name);
    trajectory.Build(X_curr_, true);

    kdl_trajectories::TrajProperties traj_properties_;
    traj_properties_.play_traj = true;
    trajectory.updateTrajectory(traj_properties_,0.001);
    traj_properties_.play_traj = false;
    traj_properties_.jogging = true;
    trajectory.updateTrajectory(traj_properties_,0.001);
    traj_properties_.jogging = false;
    traj_properties_.gain_tunning = true;
    traj_properties_.move = true;
    traj_properties_.index = 0;
    traj_properties_.amplitude = 0.1;
    trajectory.updateTrajectory(traj_properties_,0.001);
    traj_properties_.index = 1;
    trajectory.updateTrajectory(traj_properties_,0.001);
    traj_properties_.index = 2;
    trajectory.updateTrajectory(traj_properties_,0.001);
    traj_properties_.index = 3;
    trajectory.updateTrajectory(traj_properties_,0.001);
    traj_properties_.index = 4;
    trajectory.updateTrajectory(traj_properties_,0.001);
    traj_properties_.index = 5;
    trajectory.updateTrajectory(traj_properties_,0.001);
    traj_properties_.gain_tunning = false;
    traj_properties_.move = false;
    trajectory.updateTrajectory(traj_properties_,0.001);
    trajectory.publishTrajectory();
    trajectory.DirectionOfMotion(0,0.01);
}